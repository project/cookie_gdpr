<?php

namespace Drupal\cookie_gdpr\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides cookie_gdpr block.
 *
 * @Block(
 *   id = "cookie_gdpr",
 *   admin_label = @Translation("Cookie GDPR"),
 *   category = @Translation("Blocks")
 * )
 */
class CookieBlock extends BlockBase {
  

  /**
   * {@inheritdoc}
   */
  public function build() {
    return array(
      '#type' => 'markup',
      '#markup' => '<div id="cookiemsg"></div>',
      '#attached' => array(
        'library' => array(
          'cookie_gdpr/cookie_gdpr-styling',
        ),
      ),
    );
  }

}
