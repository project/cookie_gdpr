 CONTENTS OF THIS FILE
---------------------  
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------
 * GDPR Cookie Alert provide you to user cookies alert that allow your website cookie data access policy.

 * What is different this module from the EU Cookie Compliance one?
   This is a simple custom block module for Cookie data access policy.It's place only footer position regions. Then Geoip/smartip 
   features excluded from this module.Basically It's very simple javascript based cookie explicit access warning message module.Wherever 
   we ensure to user about the cookie data acces policy.
 
 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/cookie_gdpr

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/cookie_gdpr

REQUIREMENTS
------------
 *No requirements required for this module.

INSTALLATION
------------
 
  *Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/docs/8/extending-drupal/installing-contributed-modules
   for further information.


CONFIGURATION
-------------
 *No configuration required for this module

MAINTAINERS
-----------
Current maintainers:
 * kalyan-samanta -https://www.drupal.org/u/kalyansamanta
